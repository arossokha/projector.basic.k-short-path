import tracemalloc
from functools import wraps
from time import time

from lib.core import DirectedGraph


def performance(func):
    @wraps(func)
    def _time_it(*args, **kwargs):
        start = int(round(time() * 1000))
        tracemalloc.start()
        graph = None
        try:
            graph = func(*args, **kwargs)

            return graph
        finally:
            current, peak = tracemalloc.get_traced_memory()
            if isinstance(graph, DirectedGraph):
                print(
                    "\n\033[37mGraph                 :\033[35;1m nodes = {n} edges = {e}\033[0m".format(
                        n=graph.get_node_count(), e=graph.get_edge_count()))
            print("\033[37mMemory usage          :\033[36m {}MB\033[0m".format(current / 10 ** 6))
            print("\033[37mPeak                  :\033[36m {}MB\033[0m".format(peak / 10 ** 6))
            tracemalloc.stop()
            print("Total execution time: {e} ms".format(e=int(round(time() * 1000)) - start))

    return _time_it
