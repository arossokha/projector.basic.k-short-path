import heapq
from collections import deque
from math import inf
from typing import Optional


class Edge:
    def __init__(self, from_, to, weight=1):
        self.from_ = from_
        self.to = to
        self.weight = weight


class RouteEdge(Edge):
    def __init__(self, depart, arrival, depart_date=None, arrival_date=None, price=None,
                 changes=None, pos=None):
        super().__init__(depart, arrival, price)
        self.depart = depart
        self.arrival = arrival
        self.price = price
        self.depart_date = depart_date
        self.arrival_date = arrival_date
        self.changes = changes
        self.pos = pos


class DirectedGraph:

    def __init__(self):
        self.graph = {}
        self._unique_nodes = set()
        self._edges = {}

    def get_edge_count(self) -> int:
        return len(self._edges)

    def get_node_count(self) -> int:
        return len(self._unique_nodes)

    def get_nodes(self):
        return list(self._unique_nodes)

    def add_edge(self, edge: Edge):
        self._unique_nodes.add(edge.from_)
        self._unique_nodes.add(edge.to)

        if edge.from_ not in self.graph:
            self.graph[edge.from_] = []

        self.graph[edge.from_].append(edge)
        self._edges[self._prepare_edge_unique_from(edge)] = edge

    def _prepare_edge_unique_from(self, edge) -> str:
        return str(edge.from_) + '_' + str(edge.to)

    def get(self, from_) -> list:
        return self.graph.get(from_, [])

    def print(self, from_):
        print("Print child for {} node".format(from_))

        for edge in self.get(from_):
            print("From {f} to {t} weight {w}".format(
                f=edge.from_, t=edge.to, w=edge.weight))

    def find_shot_path(self, start, end):
        cost = self._dijkstra_algorithm(start, end)
        if cost == 0 or cost == inf:
            return ShortPath(self, [], cost)
        path = self.__extract_short_path(end)
        return ShortPath(self, path, cost)

    def k_shortest_paths(self, source, dest, k=1):
        if source == dest:
            return []
        cost = self._dijkstra_algorithm(source, dest)
        if cost == inf:
            # shortest path not found
            return []
        costs = [cost]
        path = self.__extract_short_path(dest)
        paths = [path]
        founded_short_paths = [(cost, path)]
        while founded_short_paths or len(paths) >= k:
            edges_removed = []
            for j in range(len(paths[-1]) - 1):
                spur_node = paths[-1][j]
                root_path = paths[-1][:j + 1]

                for current_path in paths:
                    #  remove first J nodes from root_path
                    if len(current_path) > j and root_path == current_path[:j + 1]:
                        u = current_path[j]
                        v = current_path[j + 1]
                        if self.has_edge(u, v):
                            removed_edge = self.remove_edge(u, v)
                            edges_removed.append(removed_edge)

                # for each node rootPathNode in rootPath except spurNode:
                #     remove rootPathNode from Graph;

                spur_path_cost = self._dijkstra_algorithm(spur_node, dest)
                spur_path = self.__extract_short_path(dest)

                if spur_path_cost != inf:
                    total_path = root_path[:-1] + spur_path
                    total_path_cost = self.calculate_cost_by_path(root_path,
                                                                  edges_removed) + spur_path_cost
                    heapq.heappush(founded_short_paths, (total_path_cost, total_path))

            for edge in edges_removed:
                self.add_edge(edge)

            if not founded_short_paths:
                break

            while len(founded_short_paths) > 0 and path in paths:
                (cost, path) = heapq.heappop(founded_short_paths)

            if path not in paths:
                costs.append(cost)
                paths.append(path)

            if len(costs) >= k:
                break

        # prepare correct return data
        short_paths = []
        for i in range(len(costs)):
            if len(short_paths) == k:
                break
            short_paths.append(ShortPath(self, path_cost=costs[i], path=paths[i]))
        return short_paths

    def _dijkstra_algorithm(self, source, dest):
        nodes = self.get_nodes()
        distances = {node: inf for node in nodes}
        self._path_nodes = {
            node: None for node in nodes
        }

        if source == dest:
            return inf

        distances[source] = 0
        # visited = set([source])
        # nodes_to_visit = [(edge.weight, edge.to, edge.from_) for edge in self.get(source)]
        # heapq.heapify(nodes_to_visit)

        visited = set()
        nodes_to_visit = [(0, source)]

        while len(nodes_to_visit):
            distance_to_current_node, current_node = heapq.heappop(nodes_to_visit)
            visited.add(current_node)

            if dest in visited:
                # shortest path already founded
                break

            for edge in self.get(current_node):
                if edge.to in visited:
                    continue

                new_distance = edge.weight + distances[edge.from_]
                if new_distance < distances[edge.to]:
                    distances[edge.to] = round(new_distance, 2)
                    self._path_nodes[edge.to] = edge.from_

                nodes_to_visit.append((edge.weight, edge.to))

            heapq.heapify(nodes_to_visit)

        return distances[dest] if dest in distances else inf

    def __extract_short_path(self, dest) -> list:
        path, current_node = deque(), dest
        while self._path_nodes[current_node] is not None:
            path.appendleft(current_node)
            current_node = self._path_nodes[current_node]
        if path:
            path.appendleft(current_node)
        return list(path)

    def has_edge(self, source, dest):
        return self._prepare_edge_unique_from(Edge(source, dest)) in self._edges

    def remove_edge(self, source, dest):
        edges = self.get(source)
        for i in range(len(edges)):
            edge = edges[i]
            if edge.to == dest:
                del edges[i]
                del self._edges[self._prepare_edge_unique_from(Edge(source, dest))]
                return edge
        return None

    def get_edge(self, source, dest) -> Optional[Edge]:
        return self._edges.get(self._prepare_edge_unique_from(Edge(source, dest)), None)

    def calculate_cost_by_path(self, root_path, removed_edges):
        cost = 0.0

        for i in range(0, len(root_path)):
            edge = self.get_edge(root_path[i], root_path[i - 1])
            if edge is None:

                for edge in removed_edges:
                    if edge.to == root_path[i] and edge.from_ == root_path[i - 1]:
                        cost += edge.weight
                        break
            else:
                cost += edge.weight

        return cost


class RouteDirectedGraph(DirectedGraph):
    def k_shortest_paths(self, source, dest, k=1):
        """
        @TODO: update algorithm according that edge data contains departure and arrival date time
        """
        return super(RouteDirectedGraph, self).k_shortest_paths(source, dest, k)

    def _dijkstra_algorithm(self, source, dest):
        # slow realization
        nodes = self.get_nodes()
        distances = {node: inf for node in nodes}
        self._path_nodes = {
            node: None for node in nodes
        }

        if source == dest:
            return inf

        distances[source] = 0
        while nodes:
            current_node = min(nodes, key=lambda node: distances.get(node, inf))
            # remove from nodes that we need to visit
            nodes.remove(current_node)
            # stop if we can't found smallest distance
            if distances[current_node] == inf:
                break

            # process all child for node
            for edge in self.get(current_node):
                cost = edge.weight
                child = edge.to

                route_cost = distances[current_node] + cost
                # update route cost for current node
                if route_cost < distances.get(child, inf):
                    distances[child] = round(route_cost, 2)
                    self._path_nodes[child] = current_node

        return distances[dest] if dest in distances else inf

    def print(self, from_):
        for edge in self.get(from_):
            print("From {f} to {t} price {p} depart {d} arrive {a} changes {c}".format(
                f=edge.from_, t=edge.to, p=edge.price, d=edge.depart_date, a=edge.arrival_date,
                c=edge.changes))


class ShortPath:

    def __init__(self, graph: DirectedGraph, path=None, path_cost: float = 0.0):
        self.graph = graph
        self.path = path if path is not None else []
        self.path_cost = path_cost

    def print(self):
        print(str(self))

    def __str__(self):
        return str(self.path_cost) + ': ' + '->'.join(map(str, self.path))


def load_graph_from_file(file_name: str) -> DirectedGraph:
    graph = DirectedGraph()
    with open(file_name) as f:
        edge_count = int(f.readline().strip())
        for _ in range(edge_count):
            from_, to, w = list(map(int, f.readline().rstrip().split()))
            edge = Edge(from_, to, w)
            graph.add_edge(edge)

    return graph


if __name__ == '__main__':
    graph = load_graph_from_file('../small_graph.input')

    graph.print(0)
    graph.print(1)
    graph.print(2)

    print(graph.find_shot_path(0, 17))
    print(graph.find_shot_path(0, 16))

    print('All short paths')
    short_paths = graph.k_shortest_paths(0, 16, 10)
    if len(short_paths) > 0:
        for short_path in short_paths:
            short_path.print()
    else:
        print('No short paths found')
