# https://stackoverflow.com/questions/20133479/how-to-draw-directed-graphs-using-networkx-in-python
# https://www.programcreek.com/python/example/52639/networkx.Graph
# Drawing documentation
# https://networkx.github.io/documentation/stable/reference/drawing.html
# https://networkx.github.io/documentation/stable/auto_examples/drawing/plot_random_geometric_graph.html#sphx-glr-auto-examples-drawing-plot-random-geometric-graph-py
import random

import matplotlib.pyplot as plt
import networkx as nx

from lib.core import DirectedGraph


def draw_short_paths(
        short_paths: list,
        figsize=(10, 10),
        node_size=1500,
        rand_edge_color=True
):
    directed_graph = nx.DiGraph()

    for short_path in short_paths:
        path = short_path.path
        for i in range(1, len(path)):
            u = path[i - 1]
            v = path[i]
            edge = short_path.graph.get_edge(u, v)
            directed_graph.add_edge(u, v, weight=edge.weight)

    values = [node if isinstance(node, int) else hash(node) for node in directed_graph.nodes()]

    colored_edges = {}
    for short_path in short_paths:
        path = short_path.path
        if rand_edge_color:
            r = random.random()
            b = random.random()
            g = random.random()
            color = (r, g, b)
        else:
            color = 'red'
        for i in range(1, len(path)):
            colored_edges[(path[i - 1], path[i])] = color

    edge_colors = []
    for edge in directed_graph.edges():
        edge_colors.append(colored_edges.get(edge, 'black'))

    pos = nx.circular_layout(directed_graph)

    edge_labels = dict([((u, v,), d['weight']) for u, v, d in directed_graph.edges(data=True)])

    plt.figure(figsize=figsize)
    nx.draw_networkx_edge_labels(directed_graph, pos, edge_labels=edge_labels)

    nx.draw(directed_graph, pos,
            with_labels=True,
            node_color=values,
            node_size=node_size,
            edge_color=edge_colors,
            arrows=True)

    plt.show()


def draw_short_paths_on_graph(
        graph: DirectedGraph,
        short_paths: list,
        figsize=(7, 7),
        node_size=1500,
        rand_edge_color=True
):
    directed_graph = nx.DiGraph()

    for node in graph.graph.keys():
        for edge in graph.get(node):
            directed_graph.add_edge(edge.from_, edge.to, weight=edge.weight)

    values = [node if isinstance(node, int) else hash(node) for node in directed_graph.nodes()]

    colored_edges = {}
    for short_path in short_paths:
        path = short_path.path
        if rand_edge_color:
            r = random.random()
            b = random.random()
            g = random.random()
            color = (r, g, b)
        else:
            color = 'red'
        for i in range(1, len(path)):
            colored_edges[(path[i - 1], path[i])] = color

    edge_colors = []
    for edge in directed_graph.edges():
        edge_colors.append(colored_edges.get(edge, 'black'))

    pos = nx.circular_layout(directed_graph)

    edge_labels = dict([((u, v,), d['weight']) for u, v, d in directed_graph.edges(data=True)])

    plt.figure(figsize=figsize)
    nx.draw_networkx_edge_labels(directed_graph, pos, edge_labels=edge_labels)

    nx.draw(directed_graph, pos,
            with_labels=True,
            node_color=values,
            node_size=node_size,
            edge_color=edge_colors,
            cmap=plt.cm.Blues,
            arrows=True)

    plt.show()


def draw_digraph_with_weight(graph: DirectedGraph, figsize=(50, 50), node_size=500):
    directed_graph = nx.DiGraph()

    for node in graph.graph.keys():
        for edge in graph.get(node):
            directed_graph.add_edge(edge.from_, edge.to, weight=edge.weight)

    values = [node if isinstance(node, int) else hash(node) for node in directed_graph.nodes()]
    edge_colors = ['black' for edge in directed_graph.edges()]

    pos = nx.kamada_kawai_layout(directed_graph)

    edge_labels = dict([((u, v,), d['weight']) for u, v, d in directed_graph.edges(data=True)])

    plt.figure(figsize=figsize)
    nx.draw_networkx_edge_labels(directed_graph, pos, edge_labels=edge_labels)

    nx.draw(directed_graph, pos,
            with_labels=True,
            node_color=values,
            node_size=node_size,
            edge_color=edge_colors,
            arrows=True)

    plt.show()


if __name__ == '__main__':
    G = nx.DiGraph()

    G.add_edges_from([('A', 'B'), ('C', 'D'), ('G', 'D')], weight=1)
    G.add_edges_from([('D', 'A'), ('D', 'E'), ('B', 'D'), ('D', 'E')], weight=2)
    G.add_edges_from([('B', 'C'), ('E', 'F')], weight=3)
    G.add_edges_from([('C', 'F')], weight=4)

    val_map = {'A': 0.1,
               'D': 0.5714285714285714,
               'H': 0.2}

    values = [val_map.get(node, 0.45) for node in G.nodes()]

    edge_labels = dict([((u, v,), d['weight'])
                        for u, v, d in G.edges(data=True)])
    red_edges = [('C', 'D'), ('D', 'A')]
    edge_colors = ['black' if not edge in red_edges else 'red' for edge in G.edges()]

    pos = nx.spring_layout(G)
    nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels)
    nx.draw(G, pos, with_labels=True, node_color=values, node_size=1500, edge_color=edge_colors,
            arrows=True)
    plt.figure(figsize=(10,10))
    plt.xlim(-0.05, 1.05)
    plt.ylim(-0.05, 1.05)
    plt.axis('off')
    plt.show()
