# Projector: Algorithms basics 10 group 
## Course project: K-shortest paths

This repository is course project for Projector school

Here you can find my implementation of "Yen's algorithm"

[My presentation](https://docs.google.com/presentation/d/1nDoYvUudaIl9sAwRbeYwxyZArbpFzyj5xTLm99WrGNY/edit?usp=sharing)

["Yen's algorithm" on wiki](https://en.wikipedia.org/wiki/Yen's_algorithm)

### Files

* main_big_graph.py - use real data for build graph
* main_small_graph.py - small graph for testing with drawing
* main_performance.py - performance tests of algorithm 

### How to use
Best way to run project is use [PyCharm](https://www.jetbrains.com/pycharm/)

**Python** version 3.5+