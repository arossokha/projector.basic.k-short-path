import csv
import json
from concurrent.futures.thread import ThreadPoolExecutor
from urllib import request
from urllib.error import HTTPError

from lib.db import get_connection


def get_iata_codes():
    COL_IDX_IATA = 4
    with open('iata_codes.csv', 'r', encoding="utf8") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            if len(row[COL_IDX_IATA]) == 3:
                yield row[COL_IDX_IATA]


def load_json(link: str):
    try:
        json_str = request.urlopen(link).read().decode('utf-8')
    except HTTPError as e:
        return {}

    return json.loads(json_str)


def print_directions_by_iata(iata):
    supported_directions = load_json(
        "http://map.aviasales.ru/supported_directions.json?origin_iata={iata}&one_way=false&locale=en".format(
            iata=iata))

    if 'directions' not in supported_directions:
        return []

    directions = set()
    for direction in supported_directions['directions']:
        print(direction)
        directions.add(direction)
    return directions


def load_prices_for_year(origin_iata: str, con):
    c = 0
    with ThreadPoolExecutor(max_workers=12) as executor:
        for month in range(1, 13):
            future = executor.submit(load_month_prices, month, origin_iata, con)
            c += future.result()

    # for month in range(1, 13):
    #     c += load_month_prices(month, origin_iata, con)

    print('Len = {c}'.format(c=c))

    con.commit()


def load_month_prices(month, origin_iata, con):
    c = 0
    prices_data = load_json(
        "http://map.aviasales.ru/prices.json?origin_iata={origin_iata}&direct=1&period=2020-{month}-01:month&locale=en&currency=EUR".format(
            origin_iata=origin_iata, month=str(month).zfill(2)))
    for price_data in prices_data:
        print(price_data)
        if True or price_data['number_of_changes'] == '0':
            c += 1
            con.cursor().execute(
                'INSERT INTO prices(depart, arrival, depart_date, arrival_date, price, currency, changes)' +
                ' VALUES (?,?,?,?,?,?,?)',
                (
                    price_data['origin'],
                    price_data['destination'],
                    price_data['depart_date'],
                    price_data['return_date'],
                    float(price_data['value']),
                    'EUR',
                    price_data['number_of_changes']
                )
            )
    return c


if __name__ == '__main__':
    con = get_connection()

    cursorObject = con.cursor()

    cursorObject.execute('DROP TABLE prices')
    cursorObject.execute(
        'create table prices (id integer constraint prices_pk primary key autoincrement,' +
        'depart char(3), arrival char(3),depart_date  char(20), arrival_date char(20), ' +
        'price float not null, currency char(3), changes integer);')
    con.commit()

    with ThreadPoolExecutor(max_workers=20) as executor:
        for iata in get_iata_codes():
            print('Run task with iata: %s' % iata)
            # future = executor.submit(print_directions_by_iata, iata)
            future = executor.submit(load_prices_for_year, iata, con)
            print(' ' + str(future.result()))

    exit()
