from lib.core import RouteEdge, RouteDirectedGraph
from lib.db import get_connection
from lib.draw import draw_short_paths
from lib.utils import performance

IDX_ID = 0
IDX_FROM = 1
IDX_TO = 2
IDX_D_DATE = 3
IDX_A_DATE = 4
IDX_PRICE = 5
IDX_CURRENCY = 6
IDX_CHANGES = 7


def create_graph_from_db() -> RouteDirectedGraph:
    graph = RouteDirectedGraph()
    con = get_connection()
    cur = con.cursor()
    cur.execute(
        "SELECT id, depart, arrival, depart_date, arrival_date, MIN(price), currency, changes FROM prices WHERE changes = 0 GROUP BY depart, arrival")
    rows = [1]
    while len(rows) > 0:
        rows = cur.fetchmany(100)
        for row in rows:
            # print(row)
            graph.add_edge(RouteEdge(
                row[IDX_FROM],
                row[IDX_TO],
                price=round(row[IDX_PRICE], 2),
                depart_date=row[IDX_D_DATE],
                arrival_date=row[IDX_A_DATE],
                changes=row[IDX_CHANGES],
            ))

    return graph


@performance
def search_on_graph_k_short_paths(from_=None, to=None):
    graph = create_graph_from_db()
    graph.find_shot_path(from_, to).print()
    print('All short paths for "{f}" to "{t}"'.format(f=from_, t=to))
    short_paths = graph.k_shortest_paths(from_, to, 5)
    if len(short_paths) == 0:
        print('No short paths found')
        return []

    for short_path in short_paths:
        short_path.print()

    return short_paths


if __name__ == '__main__':
    short_paths = search_on_graph_k_short_paths('IEV', 'NYC')

    draw_short_paths(short_paths)
