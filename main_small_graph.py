from lib.core import load_graph_from_file
from lib.draw import draw_short_paths_on_graph

if __name__ == '__main__':

    graph = load_graph_from_file('small_graph.input')

    print('All short paths')
    short_paths = graph.k_shortest_paths(0, 16, 5)
    if len(short_paths) > 0:
        for short_path in short_paths:
            short_path.print()
    else:
        print('No short paths found')

    draw_short_paths_on_graph(graph, short_paths, figsize=(7, 7), node_size=1500)
