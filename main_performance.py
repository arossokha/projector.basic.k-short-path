from lib.core import load_graph_from_file
from lib.utils import performance


@performance
def print_short_paths_from_file(filename, fr, to, k):
    graph = load_graph_from_file(filename)
    print('All short paths')
    short_paths = graph.k_shortest_paths(fr, to, k)
    if len(short_paths) > 0:
        for short_path in short_paths:
            short_path.print()
    else:
        print('No short paths found')

    return graph


if __name__ == '__main__':
    print_short_paths_from_file('graph_100_1000.input', 0, 15, 5)
    print_short_paths_from_file('graph_100_2000.input', 0, 15, 5)
    print_short_paths_from_file('graph_100_4000.input', 0, 26, 5)
    print_short_paths_from_file('graph_100_8000.input', 0, 99, 5)

    for to in range(1, 10):
        print("For data {}".format(to))
        print_short_paths_from_file('graph_100_2000.input', 0, 15, to)
