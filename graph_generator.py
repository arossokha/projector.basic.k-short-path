from random import Random

import networkx as nx


def create_weight_graph(node_count=10, edge_count=20, weight_limit=20, seed=None):
    if seed is None:
        seed = Random()
    else:
        seed = Random(seed)

    G = nx.gnm_random_graph(node_count, edge_count, directed=True)
    for (u, v, w) in G.edges(data=True):
        w['weight'] = int(1 + seed.random() * weight_limit)
    return G


def create_weight_graph_random_edges(node_count=10, limit=20, alpha=0.3, seed=None):
    if seed is None:
        seed = Random()
    else:
        seed = Random(seed)

    G = nx.gnp_random_graph(node_count, alpha, directed=True)
    for (u, v, w) in G.edges(data=True):
        w['weight'] = int(1 + seed.random() * limit)
    return G


def write_graph_to_file_with_name(filename, graph: nx.MultiGraph):
    with open(filename, 'w+') as file:
        lines = [str(graph.number_of_edges()) + '\n']
        for u, v, attr in graph.edges(data=True):
            lines.append('{u} {v} {w}\n'.format(u=u, v=v, w=attr['weight']))

        file.writelines(lines)


def write_graph_to_file(graph: nx.DiGraph):
    filename = 'graph_' + str(len(graph.nodes)) + '_' + str(len(graph.edges)) + '.input'
    print('Generate file {}'.format(filename))
    with open(filename, 'w+') as file:
        lines = [str(graph.number_of_edges()) + '\n']
        for u, v, attr in graph.edges(data=True):
            lines.append('{u} {v} {w}\n'.format(u=u, v=v, w=attr['weight']))

        file.writelines(lines)


if __name__ == '__main__':
    write_graph_to_file_with_name(
        'graph_100_rand.input',
        create_weight_graph_random_edges(node_count=100, limit=20)
    )
    write_graph_to_file_with_name(
        'graph_200_rand.input',
        create_weight_graph_random_edges(node_count=200, limit=20)
    )
    write_graph_to_file_with_name(
        'graph_1000_rand.input',
        create_weight_graph_random_edges(node_count=1000, limit=20)
    )

    write_graph_to_file(create_weight_graph(node_count=100, edge_count=1000, weight_limit=100))
    write_graph_to_file(create_weight_graph(node_count=100, edge_count=2000, weight_limit=100))
    write_graph_to_file(create_weight_graph(node_count=100, edge_count=4000, weight_limit=100))
    write_graph_to_file(create_weight_graph(node_count=100, edge_count=8000, weight_limit=100))
